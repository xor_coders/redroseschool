<?php
App::uses('AppController', 'Controller');

class GeneralsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_edit() {
		$data = $this->General->find('first');
		if(empty($data)) {
			throw new NotFoundException(__('Invalid general'));
		}
		$id = $data['General']['id'];

		if ($this->request->is(array('post', 'put'))) {
			if (!empty($this->request->data['General']['online_admission_file'])) {
                if (!empty($this->request->data['General']['online_admission_file']['name'])) {
	                $file_name = $this->_upload_file($this->request->data['General']['online_admission_file'], 'application_instr');
	                $this->request->data['General']['online_admission_file'] = $file_name;
	            } else {
                    $options = array('conditions' => array('General.' . $this->General->primaryKey => $id));
                    $data = $this->General->find('first', $options);
                    $this->request->data['General']['online_admission_file'] = $data['General']['online_admission_file'];
                }
            } else {
                unset($this->request->data['General']['online_admission_file']);
            }

			$this->General->id = $id;
			if ($this->General->save($this->request->data)) {
				$this->Session->setFlash(__('The general settings has been saved.'));
				return $this->redirect(array('action' => 'edit'));
			} else {
				$this->Session->setFlash(__('The general settings could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $data;
		}
	}
}
