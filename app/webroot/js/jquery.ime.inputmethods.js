( function ( $ ) {
	'use strict';

	$.extend( $.ime.sources, {
		'bn-avro': {
			'name': 'অভ্র',
			'source': 'rules/bn/bn-avro.js'
		},
		'bn-nkb': {
			'name': 'ইউনিজয়',
			'source': 'rules/bn/bn-nkb.js'
		}                
	} );

	$.extend( $.ime.languages, {
		'bn': {
			'autonym': 'বাংলা',
			'inputmethods': ['bn-avro','bn-nkb' ]
		}
	} );

}( jQuery ) );
