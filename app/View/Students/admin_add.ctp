<?php echo $this->element('menu');?>
<?php 
$current_year = date('Y');
$starting_year = 2016;
$blood_groups = array(
    'Unknown' => 'Select Blood Group',
	'A+' => 'A+',
	'B+' => 'B+',
	'AB+' => 'AB+',
	'O+' => 'O+',
	'A-' => 'A-',
	'B-' => 'B-',
	'AB-' => 'AB-',
	'O-' => 'O-'
);
?>

<div class="index col-md-10 col-sm-10">
    <div class="white">
        <?php echo $this->Form->create('Student',array('class'=>'form-horizontal col-md-6', 'type'=>'file')); ?>
        <fieldset>
            <legend><?php echo __('Admin Add Student'); ?></legend>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Class</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('class_name_id',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Session</label>
                <div class="col-sm-9">
                    <select name="data[Student][session]" class="form-control">
                    	<?php for ($i=$current_year; $i<=$current_year+1; $i++) { 
                    		echo "<option value='$i'>$i</option>";
                    	} ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Roll</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('roll_no',array('label' => false,'type'=>'number','min'=>'0','class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Name (Bangla)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('name_bn',array('label' => false,'class'=>'form-control bangla text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Name (English)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('name_en',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Father's Name (Bangla)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('father_name_bn',array('label' => false,'class'=>'form-control bangla')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Father's Name (English)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('father_name_en',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Father's Occupation</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('father_occupation',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Father's Income</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('father_income',array('type' => 'number','min'=>'0','label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Father's Mobile</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('father_mobile',array('type' => 'tel','maxlength' => '11','label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Father's Office Mobile</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('father_office_mobile',array('type' => 'tel','maxlength' => '11','label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Mother's Name (Bangla)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('mother_name_bn',array('label' => false,'class'=>'form-control bangla text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Mother's Name (English)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('mother_name_en',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Mother's Occupation</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('mother_occupation',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Mother's Income</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('mother_income',array('type' => 'number','min'=>'0','label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Mother's Mobile</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('mother_mobile',array('type' => 'tel','maxlength' => '11','label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Nationality</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('nationality',array('value' => 'Bangladeshi','label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Religion</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('religion',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Guardian Details</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('guardian_name',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Weekness (if any)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('weekness',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Speciality (if any)</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('speciallity',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Previous School</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('previous_school',array('label' => false,'class'=>'form-control text_uppercase')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Present Address</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('present_address',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Permanent Address</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('permanent_address',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Birthday</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('birthday',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Blood Group</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('blood_group',array('label' => false,'class'=>'form-control', 'options'=>$blood_groups)); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Contact Mobile</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('mobile',array('type' => 'tel','maxlength' => '11','label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Contact Mail</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('mail',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Photo</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('image',array('label' => false,'class'=>'form-control', 'type'=>'file')); ?>
                </div>
            </div>
            <div class="form-group">
            	<label for="inputEmail3" class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <button type="submit" class="btn submit-green s-c">Submit</button>
                </div>
            </div>
        </fieldset>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
